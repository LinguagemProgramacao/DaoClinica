/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.DAO;

import br.com.VO.Paciente;
import br.com.conexao.Conexao;
import br.com.dao.interfaces.PacienteInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author patrick
 */
public class PacienteDAO implements PacienteInterface{
    Connection conn;

    public PacienteDAO() throws SQLException {
        conn = Conexao.conectar();
    }

    @Override
    public ResultSet listar(Paciente paciente) throws SQLException {
        String sql = "SELECT nome,sexo,cpf FROM Paciente";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        return rs;
    }
    
    public ArrayList listar(String cpf) throws SQLException {
        String sql = "SELECT id,nome FROM Paciente where cpf="+cpf;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        return (ArrayList) rs;
    }
    
    

    @Override
    public void cadastrar(Paciente paciente) throws SQLException {
        String sql = "INSERT INTO Paciente (nome,sexo,idade,cpf,telefone,endereco,humano,idClinica) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, paciente.getNome());
        ps.setString(2, paciente.getSexo());
        ps.setInt(3, paciente.getIdade());
        ps.setString(4, paciente.getCpf());
        ps.setString(5, paciente.getTelefone());
        ps.setString(6, paciente.getEndereco());
        ps.setBoolean(7, paciente.isHumano());
        ps.setInt(8, paciente.getIdClinica());
        
        ps.execute();
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.DAO;

import br.com.VO.Prontuario;
import br.com.conexao.Conexao;
import br.com.dao.interfaces.ProntuarioInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author patrick
 */
public class ProntuarioDAO implements ProntuarioInterface{
    Connection conn;

    public ProntuarioDAO() throws SQLException {
        conn= Conexao.conectar();
    }

    @Override
    public ResultSet mostrar(Prontuario prontuario) throws SQLException {
        String sql = "SELECT * FROM Prontuario";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
        return rs;
    }

    @Override
    public void adicionarInfo(Prontuario prontuario) throws SQLException {
        String sql = "INSERT INTO Prontuario (info) VALUES (?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, prontuario.getInfo());
        ps.execute();
    }

    @Override
    public void medicarInfo(Prontuario prontuario) throws SQLException {
        String sql = "INSERT INTO Prontuario (medicacao) VALUES (?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, prontuario.getMedicacao());
        ps.execute();
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.VO;

import br.com.anotacoes.CampoVazio;
import br.com.anotacoes.NomeCampo;
import br.com.anotacoes.NomeTabela;
import br.com.anotacoes.TipoCampo;


/**
 *
 * @author patrick
 */
@NomeTabela( nome = "Paciente", nomeClasse = "Paciente")
public class Paciente {
    
    @TipoCampo( tipo = TipoCampo.Nome.CHAVE_PRIMARIA)
    @NomeCampo(nomeBanco = "id", getter = "getId")
    @CampoVazio(emBranco = CampoVazio.Nulo.NAO_NULO)
    private int id;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "nome", getter = "nomeGet")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private String nome;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "sexo", getter = "getSexo")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private String sexo;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "idade", getter = "getIdade")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private int idade;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "cpf", getter = "getCpf")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private String cpf;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "telefone", getter = "getTelefone")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private String telefone;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "endereco", getter = "getEndereco")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private String endereco;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "humano", getter = "getHumano")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private boolean humano;
    
    @TipoCampo( tipo = TipoCampo.Nome.CHAVE_ESTRANGEIRA)
    @NomeCampo( nomeBanco = "idClinica", getter = "getIdClinica")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private int idClinica;
    
    @TipoCampo( tipo = TipoCampo.Nome.CHAVE_ESTRANGEIRA)
    @NomeCampo( nomeBanco = "idProntuario", getter = "getIdProntuario")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private int idProntuario;

    public Paciente() {
    }

    public Paciente(int id, String nome, String sexo, int idade, String cpf, String telefone, String endereco, boolean humano, int idClinica, int idProntuario) {
        this.id = id;
        this.nome = nome;
        this.sexo = sexo;
        this.idade = idade;
        this.cpf = cpf;
        this.telefone = telefone;
        this.endereco = endereco;
        this.humano = humano;
        this.idClinica = idClinica;
        this.idProntuario = idProntuario;
    }

    // Getters e Setters
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public boolean isHumano() {
        return humano;
    }

    public void setHumano(boolean humano) {
        this.humano = humano;
    }

    public int getIdClinica() {
        return idClinica;
    }

    public void setIdClinica(int idClinica) {
        this.idClinica = idClinica;
    }

    public int getIdProntuario() {
        return idProntuario;
    }

    public void setIdProntuario(int idProntuario) {
        this.idProntuario = idProntuario;
    }

    

}

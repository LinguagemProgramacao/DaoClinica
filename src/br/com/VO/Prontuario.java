/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.VO;

import br.com.anotacoes.CampoVazio;
import br.com.anotacoes.NomeCampo;
import br.com.anotacoes.NomeTabela;
import br.com.anotacoes.TipoCampo;

/**
 *
 * @author patrick
 */
@NomeTabela(nome = "Prontuario", nomeClasse = "Prontuario")
public class Prontuario {
    @TipoCampo( tipo = TipoCampo.Nome.CHAVE_PRIMARIA)
    @NomeCampo( nomeBanco = "id", getter = "getId")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private int id;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "medicacao", getter = "getMedicacao")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private String medicacao;
    
    @TipoCampo( tipo = TipoCampo.Nome.COMUM)
    @NomeCampo( nomeBanco = "info", getter = "getInfo")
    @CampoVazio( emBranco = CampoVazio.Nulo.NAO_NULO)
    private String info;

    public Prontuario() {
    }

    public Prontuario(int id, String medicacao, String info) {
        this.id = id;
        this.medicacao = medicacao;
        this.info = info;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMedicacao() {
        return medicacao;
    }

    public void setMedicacao(String medicacao) {
        this.medicacao = medicacao;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.anotacoes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author patrick
 */
@Target( ElementType.FIELD)
@Retention( RetentionPolicy.RUNTIME)
public @interface CampoVazio {
    public enum Nulo{
            NULO, NAO_NULO
    };
    Nulo emBranco () default Nulo.NULO;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author patrick
 */
public class Conexao {
    static String prefix = "jdbc:mysql://";
    static String host   = "localhost";
    static String port   = ":3306";
    static String dbName = "/ClinicaHumana";
    static String user = "root";
    static String pass = "";
    
    public static Connection conectar() throws SQLException{
        String url = prefix + host + port + dbName;
        return DriverManager.getConnection(url,user,pass);
    }
    public static Connection conectar(String db) throws SQLException{
        String url = prefix + host + port + db;
        return DriverManager.getConnection(url,user,pass);
    }
}    

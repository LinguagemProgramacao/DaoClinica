/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dao.interfaces;

import br.com.VO.Paciente;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author patrick
 */
public interface PacienteInterface {
    public ResultSet listar (Paciente paciente)throws SQLException;
    public void cadastrar (Paciente paciente)throws SQLException;
}

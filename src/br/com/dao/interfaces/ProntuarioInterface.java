/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dao.interfaces;

import br.com.VO.Prontuario;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author patrick
 */
public interface ProntuarioInterface {
    public ResultSet mostrar(Prontuario prontuario) throws SQLException;
    public void adicionarInfo(Prontuario prontuario) throws SQLException;
    public void medicarInfo (Prontuario prontuario) throws SQLException;
}
